#include "LPC17xx.h"
#include "iolpc17XX.h"
#include "system_LPC17xx.h"
#include "string.h"
#include "lusb.h"
#include "timer.h"
#include "lprintf.h"



void SystemInit(void);


#define USE_MAIN_TX
#define USE_MAIN_RX
#define USE_SEC_TX
#define USE_SEC_RX


#define MAIN_IFACE_TX_BUF_SIZE 1024
#define MAIN_IFACE_TX_BUF_CNT  2
#define MAIN_IFACE_RX_BUF_SIZE 512
#define MAIN_IFACE_RX_BUF_CNT  2

#define SECOND_IFACE_TX_BUF_SIZE 1024
#define SECOND_IFACE_TX_BUF_CNT  2
#define SECOND_IFACE_RX_BUF_SIZE 512
#define SECOND_IFACE_RX_BUF_CNT  2

#ifdef USE_MAIN_TX
    static uint32_t main_tx_buf[MAIN_IFACE_TX_BUF_SIZE*MAIN_IFACE_TX_BUF_CNT];
#endif
#ifdef USE_MAIN_RX
    static uint32_t main_rx_buf[MAIN_IFACE_RX_BUF_SIZE*MAIN_IFACE_RX_BUF_CNT];
#endif
#ifdef USE_SEC_TX
    static uint32_t sec_tx_buf[SECOND_IFACE_TX_BUF_SIZE*SECOND_IFACE_TX_BUF_CNT];
#endif
#ifdef USE_SEC_RX
    static uint32_t sec_rx_buf[SECOND_IFACE_RX_BUF_SIZE*SECOND_IFACE_RX_BUF_CNT];
#endif



#define MAX_RX_PART_CNT    10

typedef struct {
    uint32_t *buf;
    uint32_t size;
} t_rx_part;


typedef struct {
    uint32_t *buf; /* буфер для приема или передачи данных */
    uint32_t cntr; /* текущее значение тестового счетчика */
    uint32_t transf_size; /* количество запросов за которое передается буфер */
    uint32_t transf_cnt;  /* размер каждого запроса (в 32-битных словах) */
    uint32_t cur_transf;  /* номер трансфера, который будет использован для следующего обмена */
    uint8_t  tx;      /* признак идет передача или прием */
    uint8_t  iface;  /* номер интерфейса */
    uint8_t  ep_idx; /* номер конечной точки в интерфейсе */
    uint8_t  run; /* запущен ли обмен */
    uint8_t  start_req; /* запрос на запуск теста */
    uint8_t  stop_req;  /* запрос на останов теста */


    t_rx_part rx_parts[MAX_RX_PART_CNT];
    uint32_t rx_put_pos;
    uint32_t rx_get_pos;
} t_transf_st;



static t_transf_st transf[4];
static int transf_st_cnt = 0;

#define DO_FOR_ALL_TRANSF(func) for (int i=0; i < transf_st_cnt; i++) { \
    func(&transf[i]); \
    }



static void transf_reset(t_transf_st *st) {
    st->cntr = 0;
    st->cur_transf = 0;
    st->rx_get_pos = st->rx_put_pos = 0;
}

static void transf_init(t_transf_st *st, uint32_t *buf, uint8_t iface_num, uint8_t ep_num,
                        uint32_t transf_size, uint32_t transf_cnt, uint8_t tx) {
    memset(st, 0, sizeof(t_transf_st));
    st->buf = buf;
    st->iface = iface_num;
    st->ep_idx = ep_num;
    st->tx = tx;
    st->transf_cnt = transf_cnt;
    st->transf_size = transf_size;
}

static void transf_start_request(t_transf_st *st) {
    st->start_req = 1;
}

static void transf_stop_request(t_transf_st *st) {
    st->stop_req = 1;
}

static void transf_stop(t_transf_st *st) {
    /* очищаем задания, поставленные на передачу по DMA */
    if (lusb_ep_intf_get_dd_in_progress(st->iface, st->ep_idx)>0)
        lusb_ep_intf_clear(st->iface, st->ep_idx);
    st->stop_req = 0;
    st->run = 0;
}

static void transf_process(t_transf_st *st) {
    if (st->stop_req) {
        transf_stop(st);
    }

    if (st->start_req) {
        if (st->run) {
            transf_stop(st);
        }

        transf_reset(st);
        st->start_req = 0;
        st->run = 1;
    }

    if (st->run) {
        int dd_in_progr = lusb_ep_intf_get_dd_in_progress(st->iface, st->ep_idx);
        if (st->tx) {
            if ((dd_in_progr >= 0) && (dd_in_progr < st->transf_cnt)) {
                uint32_t cntr = st->cntr;
                uint32_t i;
                uint32_t *buf = &st->buf[st->transf_size*st->cur_transf];
                int res;

                /* заполняем буфер тестовым счетчиком */
                for (i=0; i < st->transf_size; i++)
                    buf[i] = cntr++;

                res = lusb_ep_intf_add_dd(st->iface, st->ep_idx, buf, st->transf_size * sizeof(buf[0]), 0);

                if (res==0) {
                    if (++st->cur_transf == st->transf_cnt)
                        st->cur_transf = 0;
                    st->cntr = cntr;
                }
            }
        } else {
            while (st->rx_get_pos != st->rx_put_pos) {
                uint32_t size = st->rx_parts[st->rx_get_pos].size/4;
                uint32_t *buf = st->rx_parts[st->rx_get_pos].buf;
                uint32_t i;
                uint32_t cntr = st->cntr;

                for (i=0; i < size; i++) {
                    if (buf[i] != cntr)  {
                        lprintf("rx (%d,%d) err: expect 0x%08X, recvd 0x%08X\n",
                                st->iface, st->ep_idx, cntr, buf[i]);
                        cntr = buf[i];
                    }
                    cntr++;
                }

                if (++st->rx_get_pos == MAX_RX_PART_CNT) {
                    st->rx_get_pos = 0;
                }

                st->cntr = cntr;
            }

            if ((dd_in_progr >= 0) && (dd_in_progr < st->transf_cnt)) {
                int res = lusb_ep_intf_add_dd(st->iface, st->ep_idx,
                                          &st->buf[st->transf_size*st->cur_transf],
                                          st->transf_size * sizeof(st->buf[0]), 0);

                if (res==0) {
                    if (++st->cur_transf == st->transf_cnt)
                        st->cur_transf = 0;
                }
            }
        }
    }
}


#ifdef LUSB_ACTIVITY_INDICATION
    typedef enum {
        USB_LED_STATE_DISCON = 0x03,
        USB_LED_STATE_FS     = 0x01,
        USB_LED_STATE_HS     = 0x02,
        USB_LED_STATE_OFF    = 0x00
    } t_usb_led_state;

    static t_usb_led_state f_led_state;

    static inline void f_led_set(t_usb_led_state state) {
        if ((state == USB_LED_STATE_OFF) || (state == USB_LED_STATE_DISCON)) {
            LPC_GPIO1->FIOSET = (1<<18);
        } else {
            LPC_GPIO1->FIOCLR = (1<<18);
        }
    }

    static void f_led_set_state(t_usb_led_state state) {
        f_led_set(state);
        f_led_state = state;
    }

    void lusb_appl_cb_activity_start(void) {
        f_led_set(USB_LED_STATE_OFF);
    }

    void lusb_appl_cb_activity_end(void) {
        f_led_set(f_led_state);
    }
#endif



/*------------------------------------------------------------------------------------------------*/
int main(void) {
    LPC_PINCON->PINMODE0 = 0xAAAAAAAA;
    LPC_PINCON->PINMODE1 = 0xAAAAAAAA;
    LPC_PINCON->PINMODE2 = 0xAAAAAAAA;
    LPC_PINCON->PINMODE3 = 0xAAAAAAAA;
    LPC_PINCON->PINMODE4 = 0xAAAAAAAA;
    LPC_PINCON->PINMODE5 = 0xAAAAAAAA;
    LPC_PINCON->PINMODE6 = 0xAAAAAAAA;
    LPC_PINCON->PINMODE7 = 0xAAAAAAAA;
    LPC_PINCON->PINMODE8 = 0xAAAAAAAA;

    clock_init();

    lprintf_uart_init(0, 115200, 8, LPRINTF_UART_PARITY_NONE, 0);

    /* пользовательский LED - просто индикация миганием раз в секунду */
    LPC_GPIO1->FIODIR |= (1<<25);
    LPC_GPIO1->FIOCLR = (1<<25);

    /* индикация USB */
    LPC_GPIO1->FIODIR |= (1<<18);
    LPC_GPIO1->FIOSET = (1<<18);


    int led_st = 0;
    t_timer tmr_led;
    timer_set(&tmr_led, CLOCK_CONF_SECOND);
    //инициализация usb
    lusb_init();
    //включаем поддяжку
    lusb_connect(1);


#ifdef USE_MAIN_TX
    transf_init(&transf[transf_st_cnt++], main_tx_buf, LUSB_STBULK_INTERFACE_NUM, LUSB_STDBULK_TX_EP_NUM,
                    MAIN_IFACE_TX_BUF_SIZE, MAIN_IFACE_TX_BUF_CNT, 1);
#endif
#ifdef USE_MAIN_RX
    transf_init(&transf[transf_st_cnt++], main_rx_buf, LUSB_STBULK_INTERFACE_NUM, LUSB_STDBULK_RX_EP_NUM,
                MAIN_IFACE_RX_BUF_SIZE, MAIN_IFACE_RX_BUF_CNT, 0);
#endif
#ifdef USE_SEC_TX
    transf_init(&transf[transf_st_cnt++], sec_tx_buf,   LUSB_IAP_NATIVE_INTERFACE_NUM, LUSB_IAP_NATIVE_TX_EP_NUM,
                SECOND_IFACE_TX_BUF_SIZE, SECOND_IFACE_TX_BUF_CNT, 1);
#endif
#ifdef USE_SEC_RX
    transf_init(&transf[transf_st_cnt++], sec_rx_buf,   LUSB_IAP_NATIVE_INTERFACE_NUM, LUSB_IAP_NATIVE_RX_EP_NUM,
                SECOND_IFACE_RX_BUF_SIZE, SECOND_IFACE_RX_BUF_CNT, 0);
#endif


    for (;;) {
        /* выполнение фоновых задач (control pipe) */
        lusb_progress();

        if (timer_expired(&tmr_led)) {
            timer_reset(&tmr_led);
            if (led_st) {
                 LPC_GPIO1->FIOCLR = (1<<25);
            } else {
                 LPC_GPIO1->FIOSET = (1<<25);
            }
            led_st = !led_st;
        }

        DO_FOR_ALL_TRANSF(transf_process);
    } //for (;;)
    return 0;
}

#ifdef LUSB_APPL_CB_DD_EVENT

//callback  на завершение DMA - вызывается в прерывании
void lusb_appl_cb_dd_event(uint8_t intf, uint8_t ep_ind, uint8_t event, t_lusb_dd_status* pDD) {
    t_transf_st *st = NULL;
    for (int i=0; (st==NULL) && (i < sizeof(transf)/sizeof(transf[0])); i++) {
        if ((intf == transf[i].iface) && (ep_ind == transf[i].ep_idx))
            st = &transf[i];
    }


    if ((st != NULL) && !st->tx) {
        if (event == LUSB_DD_EVENT_EOT) {
            st->rx_parts[st->rx_put_pos].buf = (uint32_t*)(pDD->last_addr - pDD->trans_cnt);
            st->rx_parts[st->rx_put_pos].size = pDD->trans_cnt;
            if (++st->rx_put_pos == MAX_RX_PART_CNT) {
                st->rx_put_pos = 0;
            }
        }
    }
}
#endif


//callback  на изменение состояния устройства
void lusb_appl_cb_devstate_ch(uint8_t old_state, uint8_t new_state) {
    //останавливаем сбор, если вышли из сконфигурированного состояния
    if (!(new_state & LUSB_DEVSTATE_CONFIGURED)) {
        DO_FOR_ALL_TRANSF(transf_stop_request);
    }


#ifdef LUSB_ACTIVITY_INDICATION
    if (!(new_state & LUSB_DEVSTATE_DEFAULT)) {
        f_led_set_state(USB_LED_STATE_DISCON);
    } else if (!(old_state & LUSB_DEVSTATE_DEFAULT)) {
        if (lusb_get_speed()==LUSB_SPEED_HIGH) {
            f_led_set_state(USB_LED_STATE_HS);
            lprintf("usb connected with high speed!\n");
        } else {
            f_led_set_state(USB_LED_STATE_FS);
            lprintf("usb connected with full speed!\n");
        }
    }
#endif
}






int lusb_appl_cb_custom_ctrlreq_rx(t_lusb_req* req, uint8_t* buff) {
    int res = LUSB_ERR_UNSUPPORTED_REQ;
    //обслуживаем только пользовательские запросы
    if ((req->req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_VENDOR)  {
        switch (req->request) {

        }
    }
    return res;
}



const void* lusb_appl_cb_custom_ctrlreq_tx (t_lusb_req* req, int* length) {
    const uint8_t* tx_buf = NULL;
    if ((req->req_type & USB_REQ_REQTYPE_TYPE) == USB_REQ_REQTYPE_TYPE_VENDOR) {
        switch (req->request)  {
             break;
        }
    }
    return tx_buf;
}


#ifdef LUSB_APPL_CB_INTF_ALTSET_CHANGED

void lusb_appl_cb_intf_alteset_ch(uint8_t intf, uint8_t old_altset, uint8_t new_altset) {
    lprintf("interface %d altsettings changed %d->%d\n", intf, old_altset, new_altset);
    if (new_altset==1) {
        DO_FOR_ALL_TRANSF(transf_start_request);
    } else {
        DO_FOR_ALL_TRANSF(transf_stop_request);
    }
}
#endif




