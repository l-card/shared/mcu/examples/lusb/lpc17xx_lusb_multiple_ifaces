//*****************************************************************************
//
// startup.c - Boot code for Stellaris.
//
// Copyright (c) 2005,2006 Luminary Micro, Inc.  All rights reserved.
//
// Software License Agreement
//
// Luminary Micro, Inc. (LMI) is supplying this software for use solely and
// exclusively on LMI's Stellaris Family of microcontroller products.
//
// The software is owned by LMI and/or its suppliers, and is protected under
// applicable copyright laws.  All rights are reserved.  Any use in violation
// of the foregoing restrictions may subject the user to criminal sanctions
// under applicable laws, as well as to civil liability for the breach of the
// terms and conditions of this license.
//
// THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
// OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
// LMI SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
// CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
//
// This is part of revision 852 of the Stellaris Driver Library.
//
//*****************************************************************************

// modified by Martin Thomas - don't blame Luminary if it does
// not work for you.

#define ALIAS(f) __attribute__ ((weak, alias (#f)))

//*****************************************************************************
//
// Forward declaration of the default fault handlers.
//
//*****************************************************************************
// mthomas: attribute for stack-aligment (see README_mthomas.txt)
#ifdef __GNUC__
void ResetISR(void) __attribute__((__interrupt__));
static void NmiSR(void) __attribute__((__interrupt__));
static void FaultISR(void) __attribute__((__interrupt__));
static void IntDefaultHandler(void) __attribute__((__interrupt__));
#else
void ResetISR(void);
static void NmiSR(void);
static void FaultISR(void);
static void IntDefaultHandler(void);
#endif

extern void SystemInit(void);

//*****************************************************************************
//
// The entry point for the application.
//
//*****************************************************************************
extern int main(void);

//*****************************************************************************
//
// External declaration for the interrupt handler used by the application.
//
//*****************************************************************************
void WDT_IRQHandler(void) ALIAS(IntDefaultHandler);
void TIMER0_IRQHandler(void) ALIAS(IntDefaultHandler);
void TIMER1_IRQHandler(void) ALIAS(IntDefaultHandler);
void TIMER2_IRQHandler(void) ALIAS(IntDefaultHandler);
void TIMER3_IRQHandler(void) ALIAS(IntDefaultHandler);
void UART0_IRQHandler(void) ALIAS(IntDefaultHandler);
void UART1_IRQHandler(void) ALIAS(IntDefaultHandler);
void UART2_IRQHandler(void) ALIAS(IntDefaultHandler);
void UART3_IRQHandler(void) ALIAS(IntDefaultHandler);
void PWM1_IRQHandler(void) ALIAS(IntDefaultHandler);
void I2C0_IRQHandler(void) ALIAS(IntDefaultHandler);
void I2C1_IRQHandler(void) ALIAS(IntDefaultHandler);
void I2C2_IRQHandler(void) ALIAS(IntDefaultHandler);
void SPI_IRQHandler(void) ALIAS(IntDefaultHandler);
void SSP0_IRQHandler(void) ALIAS(IntDefaultHandler);
void SSP1_IRQHandler(void) ALIAS(IntDefaultHandler);
void PLL0_IRQHandler(void) ALIAS(IntDefaultHandler);
void RTC_IRQHandler(void) ALIAS(IntDefaultHandler);
void EINT0_IRQHandler(void) ALIAS(IntDefaultHandler);
void EINT1_IRQHandler(void) ALIAS(IntDefaultHandler);
void EINT2_IRQHandler(void) ALIAS(IntDefaultHandler);
void EINT3_IRQHandler(void) ALIAS(IntDefaultHandler);
void ADC_IRQHandler(void) ALIAS(IntDefaultHandler);
void BOD_IRQHandler(void) ALIAS(IntDefaultHandler);
void USB_IRQHandler(void) ALIAS(IntDefaultHandler);
void CAN_IRQHandler(void) ALIAS(IntDefaultHandler);
void GPDMA_IRQHandler(void) ALIAS(IntDefaultHandler);
void I2S_IRQHandler(void) ALIAS(IntDefaultHandler);
void Ethernet_IRQHandler(void) ALIAS(IntDefaultHandler);
void RIT_IRQHandler(void) ALIAS(IntDefaultHandler);
void MCPWM_IRQHandler(void) ALIAS(IntDefaultHandler);
void QE_IRQHandler(void) ALIAS(IntDefaultHandler);
void PLL1Handler(void) ALIAS(IntDefaultHandler);
void USBActivity_IRQHandler(void) ALIAS(IntDefaultHandler);
void CANActivity_IRQHandler(void) ALIAS(IntDefaultHandler);

//*****************************************************************************
//
// Reserve space for the system stack.
//
//*****************************************************************************
#ifndef STACK_SIZE
#define STACK_SIZE                              512
#endif

// mthomas: added section -> alignment thru linker-script
__attribute__ ((section(".stackarea")))
static unsigned long pulStack[STACK_SIZE];

//*****************************************************************************
//
// The minimal vector table for a Cortex M3.  Note that the proper constructs
// must be placed on this to ensure that it ends up at physical address
// 0x0000.0000.
//
//*****************************************************************************
__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[191])(void) =
{
    (void (*)(void))((unsigned long)pulStack + sizeof(pulStack)),
                                            // The initial stack pointer
    ResetISR,                               // The reset handler
    NmiSR,                                  // The NMI handler
    FaultISR,                               // The hard fault handler
    FaultISR,                      // The MPU fault handler
    FaultISR,                      // The bus fault handler
    FaultISR,                      // The usage fault handler
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    NmiSR,                      // SVCall handler
    NmiSR,                      // Debug monitor handler
    0,                                      // Reserved
    NmiSR,                      // The PendSV handler
    NmiSR,                      // The SysTick handler
    WDT_IRQHandler,                         // Watchdog
    TIMER0_IRQHandler,                      // Timer 0
    TIMER1_IRQHandler,                      // Timer 1
    TIMER2_IRQHandler,                      // Timer 2
    TIMER3_IRQHandler,                      // Timer 3
    UART0_IRQHandler,                       // UART0
    UART1_IRQHandler,                       // UART1
    UART2_IRQHandler,                       // UART2
    UART3_IRQHandler,                       // UART3
    PWM1_IRQHandler,                        // PWM1
    I2C0_IRQHandler,                        // I2C0
    I2C1_IRQHandler,                        // I2C1
    I2C2_IRQHandler,                        // I2C2
    SPI_IRQHandler,                         // SPI
    SSP0_IRQHandler,                        // SSP0
    SSP1_IRQHandler,                        // SSP1
    PLL0_IRQHandler,                        // PLL0
    RTC_IRQHandler,                         // RTC
    EINT0_IRQHandler,                       // External Interrupt 0
    EINT1_IRQHandler,                       // External Interrupt 1
    EINT2_IRQHandler,                       // External Interrupt 2
    EINT3_IRQHandler,                       // External Interrupt 3
    ADC_IRQHandler,                         // ADC
    BOD_IRQHandler,                         // Brown Out Detect
    USB_IRQHandler,                         // USB
    CAN_IRQHandler,                         // CAN
    GPDMA_IRQHandler,                       // GPDMA
    I2S_IRQHandler,                         // I2S
    Ethernet_IRQHandler,                    // Ethernet
    RIT_IRQHandler,                         // Repetitive Interrupt Timer
    MCPWM_IRQHandler,                       // Motor Control PWM
    QE_IRQHandler,                          // Quadrature Encoder
    PLL1Handler,                            // PLL1
    USBActivity_IRQHandler,                 // USB Activity
    CANActivity_IRQHandler                  // CAN Activity
};

//устанавливаем Code Read Protection
#define CRP_DIS (0xFFFFFFFF)
#define CRP1    (0x12345678)
#define CRP2    (0x87654321)
#define CRP3    (0x43218765)

__attribute__ ((section(".crp_sect")))
const unsigned int crp = CRP_DIS;


/*#include "dv102_config.h"
__attribute__ ((section(".soft_ver")))
const unsigned int  softver = SOFT_VERSION;*/

//*****************************************************************************
//
// The following are constructs created by the linker, indicating where the
// the "data" and "bss" segments reside in memory.  The initializers for the
// for the "data" segment resides immediately following the "text" segment.
//
//*****************************************************************************
extern unsigned long _data;
extern unsigned long _edata;
extern unsigned long _bss;
extern unsigned long _ebss;
extern unsigned long _data_load_start;

//*****************************************************************************
//
// This is the code that gets called when the processor first starts execution
// following a reset event.  Only the absolutely necessary set is performed,
// after which the application supplied main() routine is called.  Any fancy
// actions (such as making decisions based on the reset cause register, and
// resetting the bits in that register) are left solely in the hands of the
// application.
//
//*****************************************************************************


void
ResetISR(void)
{
    unsigned long *pulSrc, *pulDest;

    SystemInit();

    //
    // Copy the data segment initializers from flash to SRAM.
    //
    pulSrc = &_data_load_start;
    for(pulDest = &_data; pulDest < &_edata; )
    {
        *pulDest++ = *pulSrc++;
    }

    //
    // Zero fill the bss segment.
    //
    for(pulDest = &_bss; pulDest < &_ebss; )
    {
        *pulDest++ = 0;
    }

    //
    // Call the application's entry point.
    //
    main();
}

//*****************************************************************************
//
// This is the code that gets called when the processor receives a NMI.  This
// simply enters an infinite loop, preserving the system state for examination
// by a debugger.
//
//*****************************************************************************
static void
NmiSR(void)
{
    //
    // Enter an infinite loop.
    //
    while(1)
    {
    }
}

//*****************************************************************************
//
// This is the code that gets called when the processor receives a fault
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
static void
FaultISR(void)
{
    //
    // Enter an infinite loop.
    //
    while(1)
    {
    }
}

//*****************************************************************************
//
// This is the code that gets called when the processor receives an unexpected
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
static void
IntDefaultHandler(void)
{
    //
    // Go into an infinite loop.
    //
    while(1)
    {
    }
}
