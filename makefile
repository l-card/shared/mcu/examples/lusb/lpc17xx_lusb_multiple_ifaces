##############################################################################################
#
#       !!!! Do NOT edit this makefile with an editor which replace tabs by spaces !!!!    
#
##############################################################################################
# 
# On command line:
#
# make all = Create project
#
# make clean = Clean project files.
#
# To rebuild project do "make clean" and "make all".
#

##############################################################################################
# Start of default section
#

#ROOTDIR = f:/my/lcard/PRJ

OBJDIR = ./objs

LSTDIR = ./lst

DEPDIR = ./.dep


#TRGT = d:\Program\ARM\LPCXpresso_4.0.5_123\lpcxpresso\Tools\bin\arm-none-eabi-
#TRGT = d:\Program\ARM\CodeSourcery\bin\arm-none-eabi-
#TRGT = d:\Program\ARM\yagarto\bin\arm-none-eabi-
TRGT = arm-none-eabi-
CC   = $(TRGT)gcc
CP   = $(TRGT)objcopy
AS   = $(TRGT)gcc -x assembler-with-cpp
BIN  = $(CP) -O binary
HEX  = $(CP) -O ihex 


MCU  = cortex-m3

# List all default C defines here, like -D_DEBUG=1
DDEFS = 

# List all default ASM defines here, like -D_DEBUG=1
DADEFS = 

# List all default directories to look for include files here
DINCDIR = 

# List the default directory to look for the libraries here
DLIBDIR =

# List all default libraries here
DLIBS = 

#
# End of default section
##############################################################################################

##############################################################################################
# Start of user section
#

# Define project name here
PROJECT = lpc17xx_lusb_multiple_ifaces

# Define linker script file here
LDSCRIPT= ./prj/lpc1766.ld

# List all user C define here, like -D_DEBUG=1
UDEFS = NDEBUG

# Define ASM defines here
UADEFS = 


# List C source files here
SRC  = ./src/main.c \
       ./src/startup_gcc.c \
       ./src/system_LPC17xx.c \
       ./lib/lprintf/lprintf.c \
       ./lib/lprintf/lpc17xx_uart/lprintf_lpc17xx_uart.c \
       ./lib/timer/ports/lpc17xx/clock.c \
       ./lib/lusb/lusb.c \
       ./lib/lusb/lusb_core.c \
       ./lib/lusb/lusb_ctrlreq.c \
       ./lib/lusb/lusb_activity_indication.c \
       ./lib/lusb/ports/lpc17xx/lusb_ll_lpc17xx.c
     
       
DEPOBJS = 

# List ASM source files here
ASRC = 

# List all user directories here
UINCDIR = ./inc \
          ./src \
          ./lib/lprintf \
          ./lib/lusb \
          ./lib/lusb/ports/lpc17xx \
          ./lib/timer \
          ./lib/timer/ports/lpc17xx \
          ./lib/lcspec \
          ./lib/lcspec/gcc \
          ./lib/lpc17xx_pinfunc \
          ./lib/CMSIS/CM3/CoreSupport \
          ./lib/CMSIS/CM3/DeviceSupport/NXP/LPC17xx 


OBJS2 += $(OBJDIR)/lusb_descriptors.o


# List the user directory to look for the libraries here

       

# List all user libraries here
ULIBS = 

# Define optimisation level here
OPT = -O3
OPT1 = -O0

#
# End of user defines
##############################################################################################


INCDIR  = $(patsubst %,-I%,$(DINCDIR) $(UINCDIR))
LIBDIR  = $(patsubst %,-L%,$(DLIBDIR) $(ULIBDIR))
DEFS    = $(patsubst %,-D%,$(DDEFS) $(UDEFS)) #   = $(DDEFS) $(UDEFS)
ADEFS   = $(DADEFS) $(UADEFS)
#OBJS    = $(ASRC:.s=.o) $(SRC:.c=.o)
OBJS    = $(addprefix $(OBJDIR)/, $(notdir $(sort $(ASRC:.s=.o) $(SRC:.c=.o))))
LIBS    = $(DLIBS) $(ULIBS)
MCFLAGS = -mcpu=$(MCU)

ASFLAGS = $(MCFLAGS) -g -mthumb -gdwarf-2 -Wa,-amhls=$(<:.s=.lst) $(ADEFS)
CPFLAGS = $(MCFLAGS) $(OPT) -mfix-cortex-m3-ldrd -std=gnu99 -gdwarf-2 -mthumb -fomit-frame-pointer -Wall -Wstrict-prototypes -fverbose-asm -Wa,-ahlms=$(addprefix $(LSTDIR)/, $(notdir $(@:.o=.lst))) $(DEFS)
CPFLAGS1 = $(MCFLAGS) $(OPT1) -std=gnu99 -gdwarf-2 -mthumb -fomit-frame-pointer -Wall -Wstrict-prototypes -fverbose-asm -Wa,-ahlms=$(addprefix $(LSTDIR)/, $(notdir $(@:.o=.lst))) $(DEFS)
LDFLAGS = $(MCFLAGS) -nostartfiles -T$(LDSCRIPT) -Wl,-Map=$(PROJECT).map,--cref,--no-warn-mismatch $(LIBDIR) -lm -mthumb

# Generate dependency information
CPFLAGS += -MD -MP -MF .dep/$(@F).d

#
# makefile rules
#



.PHONY: all clean prj
.SECONDEXPANSION:

#
all: | $(DEPDIR) $(OBJDIR) $(LSTDIR) prj

prj: $(OBJS) $(PROJECT).elf $(PROJECT).hex $(PROJECT).bin
	$(TRGT)size $(PROJECT).elf -A
	$(CC) --version

$(OBJDIR)/lusb_descriptors.o: ./lib/lusb/lusb_descriptors.c makefile
	$(CC) -c $(CPFLAGS) -fwide-exec-charset=UTF-16LE -I . $(INCDIR) $< -o $@

$(OBJS) : $$(filter %/$$(basename $$(notdir $$@)).c, $$(SRC)) makefile ##$$(patsubst %.o,%.c, $$@)
	$(CC) -c $(CPFLAGS) -I . $(INCDIR) $< -o $@
	
%elf: $(OBJS) $(OBJS2) $(LDSCRIPT)
	$(CC) $(OBJS) $(OBJS2) $(LDFLAGS) $(LIBS) -o $@

%bin: %elf
	$(BIN) $< $@
	
%hex: %elf
	$(HEX) $< $@

$(LSTDIR):
	mkdir $(LSTDIR)
	
$(OBJDIR):
	mkdir $(OBJDIR)	

$(DEPDIR):
	mkdir $(DEPDIR) 


clean:
	-rm -f $(PROJECT).elf
	-rm -f $(PROJECT).map
	-rm -f $(PROJECT).bin
	-rm -f $(PROJECT).hex
	-rm -f $(SRC:.c=.c.bak)
	-rm -f $(ASRC:.s=.s.bak)
	-rm -fR .dep
	-rm -fR $(OBJDIR)
	-rm -fR $(LSTDIR)
	#-rm -f $(SRC:.c=.lst)
	#-rm -f $(SRC:.c=.o)

# 
# Include the dependency files, should be the last of the makefile
#
#-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)
-include $(wildcard .dep/*)

# *** EOF ***
